package com.example.demo.jpa.repository;

import com.example.demo.entity.Person;
import com.example.demo.entity.enums.Gender;
import com.example.demo.jpa.specification.PersonSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class PersonRepositoryTest {

    @Resource
    private PersonRepository personRepository;

    @Test
    public void test_personSpecification_filter() {
        Person filter = new Person();
        List<Person> result;
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertEquals(result.size(), personRepository.count());

//        testing name filter
        filter.setName("tadas");
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).getName().equals("tadas"));

        filter.setName("das");
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == 2);

        filter.setName("");
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == personRepository.count());

//        testing surname filter
        filter = new Person();
        filter.setSurname("basa");
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == 1);
        Assert.assertTrue(result.get(0).getSurname().equals("basa"));

        filter.setSurname("ba");
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == 2);

        filter.setSurname("");
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertEquals(result.size(), personRepository.count());

//        testing gender filter
        filter = new Person();
        filter.setGender(Gender.MALE);
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == 2);

        filter.setGender(Gender.FEMALE);
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == 1);

//        testing date of birth filter
        filter = new Person();
        filter.setDateOfBirth(LocalDate.of(1999, 01, 01));
        result = personRepository.findAll(new PersonSpecification(filter));
        Assert.assertTrue(result.size() == 1);
    }
}
