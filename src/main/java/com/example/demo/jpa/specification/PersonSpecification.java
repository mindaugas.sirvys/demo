package com.example.demo.jpa.specification;

import com.example.demo.entity.Person;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PersonSpecification implements Specification<Person> {
    private Person filter;

    public PersonSpecification(Person filter) {
        super();
        this.filter = filter;
    }
    
    public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
        List<Predicate> predicates = new ArrayList<>();

        if (filter.getName() != null && !filter.getName().isEmpty()) {
            predicates.add(cb.like(cb.upper(root.get("name")), "%" + filter.getName().toUpperCase() + "%"));
        }

        if (filter.getSurname() != null && !filter.getSurname().isEmpty()) {
            predicates.add(cb.like(cb.upper(root.get("surname")), "%" + filter.getSurname().toUpperCase() + "%"));
        }

        if (filter.getDateOfBirth() != null) {
            predicates.add(cb.equal(root.get("dateOfBirth"), filter.getDateOfBirth()));
        }

        if (filter.getGender() != null) {
            predicates.add(cb.equal(root.get("gender"), filter.getGender()));
        }

        return cb.and(predicates.toArray(new Predicate[0]));
    }
}